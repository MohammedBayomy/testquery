from django.db import models

# Create your models here.
# orm tab static data
class Orm_Tab_StaticData(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)

# orm SubTab StaticData
class Orm_SubTab_StaticData(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    tap = models.ForeignKey(Orm_Tab_StaticData,
                            on_delete=models.CASCADE, related_name='tap')


# orm Field StaticData
class Orm_Field_StaticData(models.Model):
    fieldName = models.CharField(max_length=255, null=True, blank=True)
    subTab = models.ForeignKey(Orm_SubTab_StaticData,
                               on_delete=models.CASCADE, related_name='subTab')

# orm Option StaticData
class Orm_Option_StaticData(models.Model):
    value = models.CharField(max_length=255, null=True, blank=True)
    org_id = models.IntegerField(null=True, blank=True)
    dashboard_id = models.IntegerField(null=True, blank=True)
    status = models.IntegerField(null=True, blank=True)
    field = models.ForeignKey(Orm_Field_StaticData,
                              on_delete=models.CASCADE, related_name='field')


# orm Language StaticData
class Orm_Language_StaticData(models.Model):
    lang = models.CharField(max_length=255, null=True, blank=True)
    value = models.CharField(max_length=255, null=True, blank=True)
    option = models.ForeignKey(Orm_Option_StaticData,
                               on_delete=models.CASCADE, related_name='option')