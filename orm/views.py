from .models import *
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db.models import Q, F, Value, Subquery, OuterRef
from django.contrib.postgres.aggregates import JSONBAgg
from django.contrib.postgres.aggregates import ArrayAgg, StringAgg
from django.contrib.postgres.fields import ArrayField, JSONField
from django.db.models.functions import JSONObject, Cast, Concat, Coalesce
from django.db.models import Count



@api_view(["GET"])
def createOrm(request):
    
    data = list(Orm_Tab_StaticData.objects.all().annotate(
        tab_name=F('name'),
        subtab_name=F('tap__name'),
        subtab_field_name=F('tap__subTab__fieldName'),
        subtab_field_option=F('tap__subTab__field__value'),
    ).values(
        'name',
        'tap__name',
        'tap__subTab__fieldName',
    ).annotate(
        # annotate any additional fields you want to include in the result
        tab_name=JSONObject(
            name = F('name'),
            subtab_name=JSONObject(
                name =F('tap__name'),
                subtab_field_name=JSONObject(
                    fieldName=Count('tap__subTab__fieldName'),
                    name = F('tap__subTab__fieldName'),
                    subtab_field_option=ArrayAgg(
                        # create a JSON object with the selected fields from Model2
                        JSONObject(
                            field_value=F('tap__subTab__field__value'),
                            field_status=F('tap__subTab__field__status'),
                        )
                    )
                )
            )
        )
    ))
    
    return Response({"data": data}, status=status.HTTP_201_CREATED)